import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import { MaterialDashboardComponent } from './material-dashboard/material-dashboard.component';


const routes: Routes = [
  { path: 'nav', component: NavComponent },
  {path: 'matdash', component:MaterialDashboardComponent},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
